<?php

/**
 * @file
 * Handles incoming requests to fire off regularly-scheduled tasks (parser jobs).
 */

/**
 * Root directory of Drupal installation.
 */
define('DRUPAL_ROOT', getcwd());

include_once DRUPAL_ROOT . '/includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

if (!isset($_GET['key'])
  || (!isset($_GET['parser_id']) && !is_numeric($_GET['parser_id']))
  || variable_get('cron_key', 'drupal') != $_GET['key'])
{
  watchdog('cron', 'Parser could not run because an invalid key was used or Parser ID value.', array(), WATCHDOG_NOTICE);
  drupal_access_denied();
}
elseif (variable_get('maintenance_mode', 0)) {
  watchdog('cron', 'Parser could not run because the site is in maintenance mode.', array(), WATCHDOG_NOTICE);
  drupal_access_denied();
}
else {
  // Load Parser API module and call parsers.
  module_load_include('module', 'parser_api');
  // Load Base parser class.
  module_load_include('php', 'parser_api', '/parser/ParserBase.class');
  // Run parser by id.
  parser_api_action_run_callback($_GET['parser_id']);
}
