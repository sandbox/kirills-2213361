<?php

/**
 * @file
 * Drush integration of parser api.
 *
 * drush parser-run - Run parser by parser id.
 */

/**
 * Implement hook_drush_help().
 */
function parser_api_drush_help($section) {
  switch ($section) {
    case 'drush:parser-run':
      return dt('Run the specified parser by id.');
  }
}

/**
 * Implement hook_drush_command().
 */
function parser_api_drush_command() {
  $items = array();

  $items['parser-run'] = array(
    'drupal dependencies' => array('parser_api'),
    'description' => 'Run the specified parser.',
    'arguments' => array(
      'pid' => 'A specific id of parser.',
    ),
    'aliases' => array('papir'),
    'examples' => array(
      'drush papir 2' => 'Run parser by Parser Id = 2.',
    ),
    'bootstrap' => 'DRUSH_BOOTSTRAP_DRUPAL_FULL'
  );

  return $items;
}

/**
 * Callback function for views-revert command.
 */
function drush_parser_api_parser_run($pid = '') {

  // Load Base parser class.
  module_load_include('php', 'parser_api', '/parser/ParserBase.class');

  if (is_numeric($pid)) {
    $parser_info = ParserBase::getParserInfo($pid);

    // include all helpers.
    ParserBase::loadHelper();

    // include Parser class.
    module_load_include('php', $parser_info->module, 'parser/' . $parser_info->class_name . '.class');

    $class_name = $parser_info->class_name;
    $parser = new $class_name();
    try {
      // Collect pages for parse.
      if ($parser->h_url->numberOfItems()) {
        while ($item = $parser->h_url->claimItem()) {
          $args = !empty($item->data['args']) ? $item->data['args'] : array();
          $parser->parseProcess($item->data['url'], $item->data['type'], $args);
          $parser->h_url->deleteItem($item);
        }
      }
      else {
        drush_log(dt('Parser Start Process'), 'ok');
        $parser->startProcess();
      }
      drush_log(dt('Parser !name is finish work', array('!name' => $parser_info->human_name)), 'ok');
    } catch (Exception $e) {
      drush_log(dt('ERROR: Caught exception: !msg', array('!msg' => $e->getMessage())), 'error');
    }
  }
  else {
    drush_log(dt('Argument of PID not set'), 'warning');
  }
}
