<?php

/**
 * @file
 * Предоставляпет набор стандартных параметров для сущностей
 * Этот набор вы можете расширить в своем модуля пронаследов этот класс.
 */


/**
 * Class Entity
 * Базовый клас для сущностей.
 * Класс хранит только структуру создаваемой сущностии не не имеет методов для работы с ней.
 */
class EntityBase extends stdClass implements IteratorAggregate {

  public function getIterator() {
    return new ArrayObject($this);
  }

  /**
   * Add propert value for entity.
   * Ex: type, uid, etc.
   *
   * @param $name
   *   Property name.
   * @param $value
   *   Property value.
   */
  public function addProperty($name, $value) {
    $this->{$name} = $value;
    return $this;
  }


  /**
   * Save field data for a new entity.
   *
   * @param $field_name
   *   Field name as 'field_my_custom_value'
   * @param $value
   *   Value for field.
   * @param null $language
   *   Language code. Default = LANGUAGE_NONE = und
   * @param int $delta
   *   Delta.
   * @param null $property_name
   *   Property name for field as 'value' or 'tid' or etc.
   *   If property equal NULL method will be try to get field schema.
   */
  public function addField($field_name, $value, $language = NULL, $delta = NULL, $property_name = NULL) {
    if (is_null($property_name)) {
      $field_info = field_info_field($field_name);
      if (!is_null($field_info)) {
        $column = array_keys($field_info['columns']);
        $property_name = $column[0];
      }
      else {
        throw new Exception($field_name . ' not found.');
      }

    }
    if (is_null($language)) {
      $language = !empty($this->language) ? $this->language : LANGUAGE_NONE;
    }
    if (!isset($delta)) {
      $delta = !empty($this->{$field_name}[$language]) ? count($this->{$field_name}[$language]) : 0;
    }

    $this->{$field_name}[$language][$delta][$property_name] = $value;
    return $this;
  }
}
