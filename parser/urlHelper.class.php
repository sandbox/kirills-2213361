<?php
/**
 * Created by PhpStorm.
 * User: kirill
 * Date: 10.12.13
 * Time: 18:11
 */



/**
 * Class parserQueue
 * Класс для работы с очередью урлов для парсинга.
 * реализовывает методы добавления и удаления.
 */
class urlHelper {
  /**
   * The name of the queue this instance is working with.
   *
   * @var string
   */
  protected $name;

  protected $current_item;

  protected $_cookie;


  public function __construct($name) {
    $this->name = $name;
  }

  public function createItem($data) {

    if (empty($data['args'])) {
      $data['args'] = array();
    }
    // During a Drupal 6.x to 7.x update, drupal_get_schema() does not contain
    // the queue table yet, so we cannot rely on drupal_write_record().
    $query = db_insert('parser_queue')
      ->fields(array(
        'name' => $this->name,
        'data' => serialize($data),
        // We cannot rely on REQUEST_TIME because many items might be created
        // by a single request which takes longer than 1 second.
        'created' => time(),
      ));
    return (bool) $query->execute();
  }

  public function numberOfItems() {
    return db_query('SELECT COUNT(item_id) FROM {parser_queue} WHERE name = :name', array(':name' => $this->name))->fetchField();
  }

  public function claimItem($lease_time = 30) {
    // Claim an item by updating its expire fields. If claim is not successful
    // another thread may have claimed the item in the meantime. Therefore loop
    // until an item is successfully claimed or we are reasonably sure there
    // are no unclaimed items left.
    while (TRUE) {
      $item = db_query_range('SELECT data, item_id FROM {parser_queue} q WHERE expire = 0 AND name = :name ORDER BY created ASC', 0, 1, array(':name' => $this->name))->fetchObject();
      if ($item) {
        // Try to update the item. Only one thread can succeed in UPDATEing the
        // same row. We cannot rely on REQUEST_TIME because items might be
        // claimed by a single consumer which runs longer than 1 second. If we
        // continue to use REQUEST_TIME instead of the current time(), we steal
        // time from the lease, and will tend to reset items before the lease
        // should really expire.
        $update = db_update('parser_queue')
          ->fields(array(
            'expire' => time() + $lease_time,
          ))
          ->condition('item_id', $item->item_id)
          ->condition('expire', 0);
        // If there are affected rows, this update succeeded.
        if ($update->execute()) {
          $item->data = unserialize($item->data);
          return $this->current_item = $item;
        }
      }
      else {
        // No items currently available to claim.
        return FALSE;
      }
    }
  }

  public function releaseItem($item) {
    $update = db_update('parser_queue')
      ->fields(array(
        'expire' => 0,
      ))
      ->condition('item_id', $item->item_id);
    return $update->execute();
  }

  public function deleteItem($item) {
    db_delete('parser_queue')
      ->condition('item_id', $item->item_id)
      ->execute();
  }

  public function createQueue() {
    // All tasks are stored in a single database table (which is created when
    // Drupal is first installed) so there is nothing we need to do to create
    // a new queue.
  }

  public function deleteQueue() {
    db_delete('parser_queue')
      ->condition('name', $this->name)
      ->execute();
  }

  public function request($url, $custom_headers = array(), $cookie = array(), $debug = FALSE) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url); // set url to post to
    curl_setopt($ch, CURLOPT_FAILONERROR, 1);
    curl_setopt ($ch, CURLOPT_VERBOSE, 2); // Отображать детальную информацию о соединении

    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // allow redirects
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // return into a variable
    curl_setopt($ch, CURLOPT_TIMEOUT, 15); // times out after 4s
    curl_setopt($ch, CURLOPT_ENCODING, 0);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

    $headers = array(
        'Connection:keep-alive',
        'User-Agent:Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36'
      ) + $custom_headers;
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers); // Set headers to above array

    $data = curl_exec($ch); // run the whole process

    if ($debug) {
      print "<pre>\n";
      print_r(curl_getinfo($ch));  // get error info
      echo "\n\ncURL error number:" .curl_errno($ch); // print error info
      echo "\n\ncURL error:" . curl_error($ch);
      print "</pre>\n";
    }
    curl_close($ch);
    return $data;
  }
}
