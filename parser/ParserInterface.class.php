<?php
/**
 * Created by PhpStorm.
 * User: kirill
 * Date: 10.12.13
 * Time: 18:12
 */


interface ParserInterface {
  function __construct();

  /**
   * Парсинг конкрутной страницы определенного типа.
   * Например для моментального отображения свежей ифнормации при заходе пользователя на нею.
   * @param $url
   * @param $args
   * @return mixed
   */
  function parseOnePage($url, $args);

  /**
   * Форма настроек для парсинга.
   * @param $form
   * @param $form_state
   * @return mixed
   */
  static function settingsForm($form, $form_state);

  /**
   * Если очередь для парсинга пустаю.
   * Начинаем парсить сначала.
   * добовляем основные (корневые) старницы в очереь.
   * @return mixed
   */
  function startProcess();

  /**
   * Парсинг страниц из очереди.
   * @param $url
   * @param $type
   * @return mixed
   */
  function parseProcess($url, $type, $args);

}