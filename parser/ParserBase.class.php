<?php


/**
 * @file
 * Provide Base class for helper parser.
 */


/**
 * Class ParserBase
 * Base class for control parse process.
 */
abstract class ParserBase {
  public static  $parser_instance;
  private static $available_parser = array();
  private static $available_helper = array();

  /**
   * Construct method
   * TODO: Created self instance for intrface.
   */
  public function __construct() {

  }


  /**
   * Load information about parser by class name.
   * @param $class_name
   *   Real parser class name.
   * @return mixed
   */
  public function loadPrarserInstance($class_name) {
    return self::$parser_instance = db_select('papi_parser', 'papip')
      ->fields('papip', array())
      ->condition('class_name', $class_name)
      ->addTag('loadParserInstance')
      ->execute()->fetchAssoc();
  }


  /**
   * Get information about current load parser.
   * @return mixed
   */
  public static function getCurrentParser() {
    return ParserBase::$parser_instance;
  }


  /**
   * Get information about parser by id.
   * @param $pid
   * @return mixed
   */
  public static function getParserInfo($pid) {
    if (!empty(self::$available_parser)) {
      foreach(self::$available_parser as $parser) {
        if ($parser->pid == $pid) return $parser;
      }
    }
    else {
      self::getAvailableParserList();
      foreach(self::$available_parser as $parser) {
        if ($parser->pid == $pid) return $parser;
      }
    }
    return FALSE;
  }


  /**
   * Get all available parsers.
   * @return array
   */
  public static function getAvailableParserList() {
    if (empty(ParserBase::$available_parser)) {
      return ParserBase::$available_parser = db_select('papi_parser', 'papi')
        ->fields('papi', array())
        ->condition('helper', 0)
        ->execute()
        ->fetchAllAssoc('class_name');
    }
    return ParserBase::$available_parser;
  }


  /**
   * Get all available helpers.
   * @return array
   */
  public static function getAvailableHelperList() {
    if (empty(ParserBase::$available_helper)) {
      return ParserBase::$available_helper = db_select('papi_parser', 'papi')
        ->fields('papi', array())
        ->condition('helper', 1)
        ->execute()
        ->fetchAllAssoc('class_name');
    }
    return ParserBase::$available_helper;
  }


  /**
   * Load (include) all helper classes.
   */
  public static function loadHelpers() {
    $helper_list = ParserBase::getAvailableHelperList();
    foreach ($helper_list as $class) {
        module_load_include('php', $class->module, 'parser/' . $class->class_name . '.class');
    }
  }


  /**
   * Load (include) all parser classes.
   */
  public static function loadParsers() {
    $parser_list = ParserBase::getAvailableParserList();
    foreach ($parser_list as $class) {
      module_load_include('php', $class->module, 'parser/' . $class->class_name . '.class');
    }
  }


  /**
   * Register parser in the system
   * TODO Реализовать обновление инфы о парсере
   * @param $parser
   */
  public static function registerParser($parser) {
      db_insert('papi_parser')->fields(
        array(
          'class_name' => $parser['class_name'],
          'module' => $parser['module_implement'],
          'helper' => isset($parser['helper']) ? $parser['helper'] : 0,
          'human_name' => $parser['human_name'],
          'status' => $parser['status'],
          'created' => REQUEST_TIME,
        )
      )->execute();
  }


  /**
   * Check if class name register in the system.
   *
   * @param $class_name
   *   Real class name.
   * @return bool
   */
  public static function existRegisterClass($class_name) {
    if (empty(ParserBase::$available_parser)) {
       ParserBase::getAvailableParserList();
    }

    if (empty(ParserBase::$available_helper)) {
      ParserBase::getAvailableHelperList();
    }

    $list = array_merge(ParserBase::$available_parser, ParserBase::$available_helper);

    return isset($list[$class_name])
      ? $list[$class_name]
      : FALSE;
  }


  /**
   * Remove parser item from DataBase.
   * @param $pid
   */
  public static function unregisterParser($pid) {
    db_delete('papi_parser')
      ->condition('pid', $pid)
      ->execute();
  }


  /**
   * Save settings for parser.
   * @param $pid
   *   Parser ID.
   * @param $data
   */
  public static function saveSettings($pid, $data) {
    variable_set('parser_api_settings_' . $pid, $data);
  }


  /**
   * Load settings for parser.
   * @param $pid
   *   Parser ID
   * @return null
   */
  public static function loadSettings($pid) {
    return variable_get('parser_api_settings_' . $pid, array());
  }


  /**
   * Get statistics about parser.
   * @param $pid
   * @return array
   */
  public static function getParserStat($pid) {
    $parser = ParserBase::getCurrentParser();
    $info = array();
    $info['queue'] = db_select('parser_queue', 'pi')
      ->fields('pi', array('item_id'))
      ->condition('name', $parser['class_name'])
      ->execute()
      ->rowCount();
    return $info;
  }
}






