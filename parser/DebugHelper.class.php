<?php


class DebugHelper {

  private static  $parser_inst;


  public static function init(WebsiteParser $parser) {
    self::$parser_inst = $parser;
    if (property_exists($parser, 'debug')) {
      $parser::$debug = TRUE;
    }
  }

  public static function availableMethods() {
    $array1 = get_class_methods(self::$parser_inst);
    if($parent_class = get_parent_class(self::$parser_inst)){
      $array2 = get_class_methods($parent_class);
      $array3 = array_diff($array1, $array2);
    }else{
      $array3 = $array1;
    }
    return $array3;
  }

  public static function availableVars() {
    return get_class_vars(self::$parser_inst);
  }

  public static function getImplements() {
    return class_implements(self::$parser_inst);
  }


  private static function getParrentClass() {
    return class_parents(self::$parser_inst);
  }


  public static function callMethod($method_name, $args = array()) {
    $return = FALSE;
    switch (count($args)) {
      case 1:
        $return = call_user_func(array(self::$parser_inst, $method_name), $args['arg1']);
        break;
      case 2:
        $return = call_user_func(array(self::$parser_inst, $method_name), $args['arg1'], $args['arg2']);
        break;
      case 3:
        $return = call_user_func(array(self::$parser_inst, $method_name), $args['arg1'], $args['arg2'], $args['arg3']);
        break;
      default:
        $return = call_user_func(array(self::$parser_inst, $method_name));
    }
    return $return;
  }

}
