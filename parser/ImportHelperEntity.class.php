<?php


/**
 * Class SmileImportHelper
 * Основной класс хелпер.
 * Реализовывает методы обработки и хранения нод и промежуточной иформации.
 * Куда скармливаем новые ноды для создания.
 * прослойка между классами сущностей.
 *
 * Разраб может расширить этот клас.
 */
class ImportHelperEntity {
  private $parser;

  function __construct($parser){
   $this->parser = $parser;
  }

  public function saveNode(EntityBase $entity) {
    node_save($entity);
    $this->entityMapAdd('node', $entity->type, $entity);
    return $entity;
  }

  public function saveTerm(EntityBase $entity) {
    taxonomy_term_save($entity);
    $this->entityMapAdd('taxonomy', 'term', $entity);
    return $entity;
  }


  public function saveFileByUrl($path, $replace = FILE_EXISTS_REPLACE) {
    $final_file_name = basename($path);
    // Get file content
    $file_content = file_get_contents($path);
    // Temporary save file
    $file = file_save_data($file_content, 'public://' . $final_file_name, $replace);
    // Add required fields
    $file->display = 1;
    // Add new item for the field.
    return (array) $file;
  }


  /**
   * Проверка должны ли мы импортировать эту ноду. или нет.
   * @param $type
   * @param $identifier
   * @return bool
   */
  function shouldImport($type, $identifier /* id | url | .. */){
    return true;
  }


  /**
   * Add entity hash to the map table.
   *
   * @param $entity_type
   *   Entity type for store  in map table.
   * @param $entity_bundle
   *   Entity bundle (type).
   * @param $data
   *   stdClass ie. EntityBase
   * @return bool
   */
  public function entityMapAdd($entity_type, $entity_bundle, EntityBase $entity) {
    $instance = field_info_instances($entity_type, $entity_bundle);

    $hash_data = array();
    $hash_data[] = $entity_bundle;
    $hash_data[] = $entity_type;

    foreach($instance as $field_name=>$field_info) {
      if (isset($entity->{$field_info['field_name']}) && !empty($entity->{$field_info['field_name']})) {
        $hash_data[] = $entity->{$field_info['field_name']};
      }
    }

    $eid = 0;
    if (isset($entity->nid)) {
      $eid = $entity->nid;
    }
    else if (isset($entity->tid)) {
      $eid = $entity->tid;
    }

    return (bool) db_insert('papi_map')
      ->fields(array(
        'eid' => $eid,
        'pid' => $this->parser->i_parser['pid'],
        'bundle' => $entity_bundle,
        'entity_type' => $entity_type,
        'hash' => md5(serialize($hash_data)),
        'created' => time(),
      ))->execute();
  }
}
