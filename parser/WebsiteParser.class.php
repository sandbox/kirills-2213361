<?php
/**
 * Created by PhpStorm.
 * User: kirill
 * Date: 10.12.13
 * Time: 18:05
 */

/**
 * Class WebsiteParser
 *
 * Класс содержит основные хелперы для парсинга контента
 */
abstract class WebsiteParser extends ParserBase {

  // Available Helpers
  public $h_url;
  public $h_import;

  // Information property.
  public $i_parser;
  public $i_settings;


  public function __construct($class_name) {
    parent::__construct();

    $this->i_parser = $this->loadPrarserInstance($class_name);
    $this->i_settings = ParserBase::loadSettings($this->i_parser['pid']);

    // Initialize helpers.
    $this->h_url = new urlHelper($this->i_parser['class_name']);
    $this->h_import = new ImportHelperEntity($this);
  }


  function downloadPageQueue($urls, $callback){
    while (!is_bool($urls)){
      $url = array_shift($urls);
      // .. download
      $callback($url, $pageContents);
    }
  }



  // helpers
} 